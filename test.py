# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 17:04:49 2017

@author: Yilun
"""

import vibes
from vibes.FRF import FRF


#MCK to ModalModel and ModalModel to FRF successfully done. 

#fn_xls = r"\\nas.ads.mwn.de\ga83wec\TUM-PC\Desktop\Python Toolbox HiWi\toolbox Umer\Dummy.xlsx"
#
#xlsc = vibes.XlsFile(fn_xls)
#sensors = xlsc.read("Sensor") # generate sensor objects from excel file
#
#DoFs = vibes.ui.Sensor.toDoF(sensors) # transfer the sensors to DoF objects
#
#DoF_Set = [1,2]
#
#M = np.zeros([2,2],float)
#C = np.zeros([2,2],float)
#K = np.zeros([2,2],float)
#
#M[0][0] = M[1][1] = 1
#C[0][0] = C[1][1] = 1
#K[0][0] = K[1][1] = 1

#M = np.matrix([[1,0],[0,1]]) 
#C = np.matrix([[1,0],[0,1]])
#K = np.matrix([[1,0],[0,1]])


#oMCK = vibes.MCKModel("hammer",M,C,K,DoF_Set) # generate a example MCK Model object
#
## -------- Generates Modalmodel from MCK model
#
#oModalModel = ModalModel()                    
#oModalModel = oMCK.toModalModel()

# -------- Calculates Rayleigh Damping if Modal model is calcualted

#oMCK.CalculateRayleighDamping(oModalModel)

#--------- Calculates mass by adding all entries of mass matrix

#mass = oMCK.calMass()
#print('The mass of system is: ',mass)

#-------- Generates FRF Model from MCK model

#Freqs = [1,2] # give some frequencies
#oFRF = oMCK.toFRF(Freqs) # convert the MCK object to FRF object

#------- Generate FRF Model from Modalmodel

#oFRF = oModalModel.ConvertoFRF(Freqs)
#print('Frequencies:',Freqs)

#-------- Does singular value decomposition.

#print(oFRF.Data[2])
#u,s,v = np.linalg.svd(oFRF.Data[2])
#print(v)

#------ Reading FRF data from file and converting it into FRF matrices

oFRF_s2 = FRF()
oFRF_s2.Name = "hammer"
FRFMatrix = oFRF_s2.ReadDataAndCreateFRFMatrices('model1_s2_1.csv',6,4,3,1,2,True)
#Check = FRFMatrix[1]
#print(Check[23][11])
CoupledFRFMatrix2 = oFRF_s2.SquareFRFForCoupledNodesOnly(6,3,4)
#CheckCoupled = CoupledFRFMatrix[1]
#print(CheckCoupled[11][11])


oFRF_s1 = FRF()
oFRF_s1.Name = "hammer"
FRFMatrix = oFRF_s1.ReadDataAndCreateFRFMatrices('model1_s1_1.csv',6,4,3,1,2,False)
#Check = FRFMatrix[1]
#print(Check[12][9])
CoupledFRFMatrix1 = oFRF_s1.SquareFRFForCoupledNodesOnly(6,3,4)
#CheckCoupled = CoupledFRFMatrix[1]
#print(CheckCoupled[1][0])


#------- Generates a structure with FRF Model

#oStrA = vibes.SubStructure("A") # generate a structure with the name A
#oStrA.addFRF(oFRF_s1) # add the FRF object to the structure
#Data1 = oStrA.System_Dynamics["hammer"].Data[1] # check some properties of substructure A
#CoupledData1 = oStrA.System_Dynamics["hammer"].CoupledData[1]


#------ Sub structure coupling

oSubStrA = vibes.SubStructure("A") # generate a structure with the name A
oSubStrA.addFRF(oFRF_s1) # add the FRF object to the structure
Data1 = oSubStrA.System_Dynamics["hammer"].Data[1] # check some properties of substructure A
#print(len(Data1[3][0:12]))
CoupledData1 = oSubStrA.System_Dynamics["hammer"].CoupledData[1]
#print(CoupledData1[5][7])

oSubStrB = vibes.SubStructure("B") # generate a structure with the name B
oSubStrB.addFRF(oFRF_s2) # add the FRF object to the structure
Data2 = oSubStrB.System_Dynamics["hammer"].Data[1] # check some properties of substructure B
#print(Data2[3][0])
CoupledData2 = oSubStrB.System_Dynamics["hammer"].CoupledData[1]
#print(CoupledData2[5][7])


#------ Coupled Structure

oStrC = vibes.Structure("C")
FinalCoupledFRFMatrix, MatrixB, FRF_Combined = oStrC.CoupleStructures(oSubStrA,oSubStrB)
#print(FinalCoupledFRFMatrix[0][0])





















