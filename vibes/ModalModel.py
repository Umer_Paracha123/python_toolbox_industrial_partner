# -*- coding: utf-8 -*-
"""
Created on Thu Oct 19 13:46:59 2017

@author: Umer Sherdil Paracha
"""
from scipy.linalg import eig
from vibes.Mode import Mode
from vibes.FRF import FRF
import numpy as np
import math as mt

class ModalModel (object):
    def __init__(self):
        self.Name = ""
        self.frequency = []
        self.eigenmodes = []
        self.nmodes = 0
        self.dampingratio = []
        self.Modes = []
              
    #Formula/Idea taken from masters thesis. Calculates FRF model from Modal Model.
    def ConvertoFRF(self, frequency):
        oFRF = FRF()
        oFRF.Name = self.Name
        oFRF.Freqs = frequency
        length = len(self.frequency)
        
        for j in range (0,len(frequency)):
            temp = np.zeros([length, length])
            for i in range (0,length):            
                numerator = np.outer(self.eigenmodes[i],self.eigenmodes[i])
                denominator = (-1*frequency[j]*frequency[j]) + (2*1j*frequency[j]*self.dampingratio[i]*self.frequency[i]) + (self.frequency[i])          
                temp = (numerator/denominator) + temp                    
            oFRF.Data[frequency[j]] = temp
        return oFRF

    def calculatemoddalmodel(self, oMCK):
         self.frequency, self.eigenmodes = eig(oMCK.StiffnessMatrix,oMCK.MassMatrix)
         self.dampingratio = self.CalculateDampingRatio(oMCK)
         size = len(self.frequency)
         for i in range (0,size):
             self.Modes.append(Mode(self.frequency[i],self.eigenmodes[i],self.dampingratio[i]))
             
             
    def CalculateDampingRatio(self, oMCK):
        length=len(self.frequency)
        dampingratio = np.zeros([length,1],complex)
        for i in range (0,length):
            modaldamping = np.dot(np.dot(self.eigenmodes[i],oMCK.DampingMatrix),np.transpose(self.eigenmodes[i]))
            modalmass = np.dot(np.dot(self.eigenmodes[i],oMCK.MassMatrix),np.transpose(self.eigenmodes[i]))
            dampingratio[i]  = modaldamping/(2*modalmass*self.frequency[i])
        return dampingratio
    


        
       
        
        
    