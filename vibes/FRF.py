# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 16:28:59 2017

@author: Yilun Sun
"""

import pandas as pd
import numpy as np
import math as mt
import matplotlib.pyplot as plt
import pylab

class FRF(object):
    def __init__(self):
        """
        Default constructor for the FRF object
        """
        self.Freqs = []
        self.Data = {}      #Dictionay in which frequency is the key and corresponding FRF matrix is the value.
        self.Name = ""
        self.DictionaryWithRotationalDOFsRemoved = {}  #Dictionary in which frequency is the key and corresponding coupled FRF matrix for couple nodes is stored as value.
        self.DictionaryWithRotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken = {}
        self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection = {}
        
    def FillData(self,DofsPerNode,NumberofNodesToBeCoupled,NumberOfReferencesPerNode,NumberofExcitationPoints,NumberofStructuresToBeCoupled):
        
        self.DofsPerNode = DofsPerNode
        self.NumberofNodesToBeCoupled = NumberofNodesToBeCoupled
        self.NumberOfReferencesPerNode = NumberOfReferencesPerNode
        self.NumberofExcitationPoints = NumberofExcitationPoints
        self.NumberofStructuresToBeCoupled = NumberofStructuresToBeCoupled
    
    
    def ReadDataAndCreateFRFMatricesS2(self,FileName,FileNameDrivingPointFRFsOfExcitationAndResponsePoint,LoadAppliedOnThisStructure):
        df = pd.read_csv(FileName)
        df_DFRF = pd.read_csv(FileNameDrivingPointFRFsOfExcitationAndResponsePoint)
        
        # In the CSV file data corresponding to each frequency at different load cases is arranged in a row wise fashion. Transpassed entries
        # makes sure that we move along the row corresponding to each load/subcase.
        iterator = 0
        TranspassedEntries = 0
        Temp = np.zeros([self.DofsPerNode*self.NumberofNodesToBeCoupled,self.NumberOfReferencesPerNode*(self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)],complex)
        
        #Important because structure of the two CSV files is different. 
        if (LoadAppliedOnThisStructure == True):
            RowNumber = self.DofsPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
            ColoumnNumber = self.NumberOfReferencesPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)

        else:
        #!!!!! Assumption: NumberofExcitationPoints = NumberofPointsatWhichFinalResponseIsMeasured
            RowNumber = self.DofsPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
            ColoumnNumber = self.NumberOfReferencesPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
            
                
        FRFMatrix = np.zeros([RowNumber,ColoumnNumber],complex)
        FrequencyColoumn = df['Freq(Hz)']
        
        
        for k in range (0,len(FrequencyColoumn)):
            SelectedRow = df_DFRF.iloc[k]
            for j in range (ColoumnNumber-self.NumberOfReferencesPerNode,ColoumnNumber):
                for i in range (0,self.DofsPerNode):
                    Magnitude = SelectedRow.iloc[1 + 2*i + TranspassedEntries]
                    PhaseinRadians = mt.radians(SelectedRow.iloc[2 + 2*i + TranspassedEntries])
                    RealPart = Magnitude * mt.cos(PhaseinRadians)
                    ImaginaryPart = Magnitude * mt.sin(PhaseinRadians)
                    FRFMatrix[RowNumber-self.DofsPerNode+i][j] = complex(RealPart,ImaginaryPart)
                TranspassedEntries = TranspassedEntries + 2*(self.DofsPerNode)
            if FrequencyColoumn[k] <= 1.00000E-105:
                FrequencyColoumn[k] = 0
            self.Data[FrequencyColoumn[k]] = FRFMatrix
            TranspassedEntries = 0
            FRFMatrix = np.zeros([RowNumber,ColoumnNumber],complex)
        
                    
#        These three loops read the CSV file and populate the FRF matrices in an orderly fashion.
        for k in range (0,len(FrequencyColoumn)):
#        for k in range (1,2): #!!!!! This loop is currently only running for frequency: 1.
            SelectedRow = df.iloc[k]
            for j in range (0,ColoumnNumber):
                for i in range (0,RowNumber - 6):
                    Magnitude = SelectedRow.iloc[1 + 2*i + TranspassedEntries]
                    PhaseinRadians = mt.radians(SelectedRow.iloc[2 + 2*i + TranspassedEntries])
                    RealPart = Magnitude * mt.cos(PhaseinRadians)
                    ImaginaryPart = Magnitude * mt.sin(PhaseinRadians) 
                    FRFMatrix[i][j] = complex(RealPart,ImaginaryPart)
                TranspassedEntries = TranspassedEntries + 2*(RowNumber - 6) #RowNumber = Total dofs in the system. 
            Temp = self.Data[FrequencyColoumn[k]]
            Temp[0:RowNumber-6,:] = FRFMatrix[0:RowNumber-6,:]
            for x in range (0,len(FRFMatrix),6):
                for y in range (0,3):
                    Temp[RowNumber-6][iterator] = Temp[x+y][ColoumnNumber-3]
                    Temp[RowNumber-5][iterator] = Temp[x+y][ColoumnNumber-2]
                    Temp[RowNumber-4][iterator] = Temp[x+y][ColoumnNumber-1]
                    iterator = iterator + 1
            self.Data[FrequencyColoumn[k]] = Temp
            Temp = np.zeros([self.DofsPerNode*self.NumberofNodesToBeCoupled,self.NumberOfReferencesPerNode],complex)
            iterator = 0
            TranspassedEntries = 0
            FRFMatrix = np.zeros([RowNumber,ColoumnNumber],complex)
            
        Temp2 = np.zeros([RowNumber,ColoumnNumber],complex)
        
        if (LoadAppliedOnThisStructure == True):
            for key in self.Data:
                FRFMatrix = self.Data[key]
                for i in range (0,ColoumnNumber):
                    if (i>=12):
                        Temp2[:,i-12] = FRFMatrix[:,i]
                    else:
                        Temp2[:,i+3] = FRFMatrix[:,i]
                    
                self.Data[key] = Temp2
                Temp2 = np.zeros([RowNumber,ColoumnNumber],complex)
                
            for key in self.Data:
                FRFMatrix = self.Data[key]
                for i in range (0,RowNumber):
                    if (i>=24):
                        Temp2[i-24,:] = FRFMatrix[i,:]
                    else:
                        Temp2[i+6,:] = FRFMatrix[i,:]
                        
                self.Data[key] = Temp2
                Temp2 = np.zeros([RowNumber,ColoumnNumber],complex)
                
            
        return self.Data
    
    def ReadDataAndCreateFRFMatricesS1(self,FileName,FileNameDrivingPointFRFsOfExcitationAndResponsePoint,LoadAppliedOnThisStructure):
        df = pd.read_csv(FileName)
        df_DFRF = pd.read_csv(FileNameDrivingPointFRFsOfExcitationAndResponsePoint)
        
        # In the CSV file data corresponding to each frequency at different load cases is arranged in a row wise fashion. Transpassed entries
        # makes sure that we move along the row corresponding to each load/subcase.
        iterator = 0
        TranspassedEntries = 0
        Temp = np.zeros([self.DofsPerNode*self.NumberofNodesToBeCoupled,self.NumberOfReferencesPerNode*(self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)],complex)

        
        #Important because structure of the two CSV files is different. 
        if (LoadAppliedOnThisStructure == True):
            RowNumber = self.DofsPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
            ColoumnNumber = self.NumberOfReferencesPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
        else:
        #!!!!! Assumption: NumberofExcitationPoints = NumberofPointsatWhichFinalResponseIsMeasured
            RowNumber = self.DofsPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
            ColoumnNumber = self.NumberOfReferencesPerNode * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
            
                
        FRFMatrix = np.zeros([RowNumber,ColoumnNumber],complex)
        FrequencyColoumn = df['Freq(Hz)']
        
        
        for k in range (0,len(FrequencyColoumn)):
            SelectedRow = df_DFRF.iloc[k]
            for j in range (ColoumnNumber-self.NumberOfReferencesPerNode,ColoumnNumber):
                for i in range (0,self.DofsPerNode):
                    Magnitude = SelectedRow.iloc[1 + 2*i + TranspassedEntries]
                    PhaseinRadians = mt.radians(SelectedRow.iloc[2 + 2*i + TranspassedEntries])
                    RealPart = Magnitude * mt.cos(PhaseinRadians)
                    ImaginaryPart = Magnitude * mt.sin(PhaseinRadians)
                    FRFMatrix[RowNumber-self.DofsPerNode+i][j] = complex(RealPart,ImaginaryPart)
                TranspassedEntries = TranspassedEntries + 2*(self.DofsPerNode)
            if FrequencyColoumn[k] <= 1.00000E-105:
                FrequencyColoumn[k] = 0
            self.Data[FrequencyColoumn[k]] = FRFMatrix
            TranspassedEntries = 0
            FRFMatrix = np.zeros([RowNumber,ColoumnNumber],complex)
        
                    
#        These three loops read the CSV file and populate the FRF matrices in an orderly fashion.
        for k in range (0,len(FrequencyColoumn)):
#        for k in range (1,2): #!!!!! This loop is currently only running for frequency: 1.
            SelectedRow = df.iloc[k]
            for j in range (0,ColoumnNumber-3):
                for i in range (0,RowNumber):
                    Magnitude = SelectedRow.iloc[1 + 2*i + TranspassedEntries]
                    PhaseinRadians = mt.radians(SelectedRow.iloc[2 + 2*i + TranspassedEntries])
                    RealPart = Magnitude * mt.cos(PhaseinRadians)
                    ImaginaryPart = Magnitude * mt.sin(PhaseinRadians) 
                    FRFMatrix[i][j] = complex(RealPart,ImaginaryPart)
                TranspassedEntries = TranspassedEntries + 2*(RowNumber) #RowNumber = Total dofs in the system. 
            TranspassedEntries = 0
            Temp = self.Data[FrequencyColoumn[k]]
            Temp[0:RowNumber,0:ColoumnNumber-3] = FRFMatrix[0:RowNumber,0:ColoumnNumber-3]
            for x in range (0,len(FRFMatrix),6):
                for y in range (0,3):
                    Temp[x+y][ColoumnNumber-3] = Temp[RowNumber-6][iterator]
                    Temp[x+y][ColoumnNumber-2] = Temp[RowNumber-5][iterator]
                    Temp[x+y][ColoumnNumber-1] = Temp[RowNumber-4][iterator]
                    iterator = iterator + 1
            self.Data[FrequencyColoumn[k]] = Temp
            iterator = 0
      
            
        return self.Data    
    
    
    def ReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection(self,LoadAppliedOnThisStructure):
        if (LoadAppliedOnThisStructure == True):
            df = pd.read_csv('model1_s2_dfrf_excitation_response_point_for_check.csv')
        else :
            df = pd.read_csv('model1_s1_dfrf_excitation_response_point_for_check.csv')
        FrequencyColoumn = df['Freq(Hz)']
        for k in range (0,len(FrequencyColoumn)):
            SelectedRow = df.iloc[k]
            Magnitude = SelectedRow.iloc[29]
            PhaseinRadians = mt.radians(SelectedRow.iloc[30])            
            RealPart = Magnitude * mt.cos(PhaseinRadians)
            ImaginaryPart = Magnitude * mt.sin(PhaseinRadians)
            if FrequencyColoumn[k] <= 1.00000E-105:
                FrequencyColoumn[k] = 0
            self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[FrequencyColoumn[k]] = complex(RealPart,ImaginaryPart)
            
        return self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection
    
    def RotationalDOFsRemoved(self,DofsPerNodeToBeCoupled,NumberofNodesToBeCoupled, LoadAppliedOnThisStructure):
        #Important because structure of the two CSV files is different. 
        if (LoadAppliedOnThisStructure == True):
            RowNumberModified = DofsPerNodeToBeCoupled * (NumberofNodesToBeCoupled + self.NumberofExcitationPoints) 
            ColoumnNumberModified = self.NumberOfReferencesPerNode * (NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
        else:
        #!!!!! Assumption: NumberofExcitationPoints = NumberofPointsatWhichFinalResponseIsMeasured
            RowNumberModified = DofsPerNodeToBeCoupled * (self.NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
            ColoumnNumberModified = self.NumberOfReferencesPerNode * (NumberofNodesToBeCoupled + self.NumberofExcitationPoints)
        
        iterator = 0
        CoupledFRFMatrix = np.zeros([RowNumberModified,ColoumnNumberModified],complex)
        
        for key in self.Data:
#        for k in range (1,2): #!!!!!! Only for frequency 1. Must change this  
            FRFMatrix = self.Data[key]   
            for i in range (0,len(FRFMatrix),6):
                for j in range (0,3):
                    CoupledFRFMatrix[iterator] = FRFMatrix[i+j][0:ColoumnNumberModified]
                    iterator = iterator + 1
            self.DictionaryWithRotationalDOFsRemoved[key] = CoupledFRFMatrix
            iterator = 0
            CoupledFRFMatrix = np.zeros([RowNumberModified,ColoumnNumberModified],complex)

        return self.DictionaryWithRotationalDOFsRemoved
    
    def RotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken(self,LoadAppliedOnThisStructure):
        CoupledFRFMatrix = np.zeros([2,2],complex)
        for key in self.Data:
            FRFMatrix = self.Data[key]
            if (LoadAppliedOnThisStructure == True):
#                Node: 270
#                CoupledFRFMatrix[0][0] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
#                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[2][14]
#                CoupledFRFMatrix[1][1] = FRFMatrix[2][2]
#                Node: 279
#                CoupledFRFMatrix[0][0] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
#                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[8][14]
#                CoupledFRFMatrix[1][1] = FRFMatrix[8][5]
#                Node: 293
                CoupledFRFMatrix[0][0] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[14][14]
                CoupledFRFMatrix[1][1] = FRFMatrix[14][8]
#                Node: 305
#                CoupledFRFMatrix[0][0] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
#                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[20][14]
#                CoupledFRFMatrix[1][1] = FRFMatrix[20][11]
            else:
#                Node: 452
#                CoupledFRFMatrix[1][1] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
#                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[26][2]
#                CoupledFRFMatrix[0][0] = FRFMatrix[2][2]
#                Node: 464
#                CoupledFRFMatrix[1][1] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
#                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[26][5]
#                CoupledFRFMatrix[0][0] = FRFMatrix[8][5]
#                Node: 483
                CoupledFRFMatrix[1][1] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[26][8]
                CoupledFRFMatrix[0][0] = FRFMatrix[14][8]
#                Node: 498
#                CoupledFRFMatrix[1][1] = self.DictionaryWithReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection[key]
#                CoupledFRFMatrix[1][0] = CoupledFRFMatrix[0][1] = FRFMatrix[26][11]
#                CoupledFRFMatrix[0][0] = FRFMatrix[20][11]
            self.DictionaryWithRotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken[key] = CoupledFRFMatrix
            CoupledFRFMatrix = np.zeros([2,2],complex)
            
        return self.DictionaryWithRotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken
            
    
    
    def PlottingIndividualFRFsForStructure2(self):
        ReferenceAcceleration = 0.00000134
        iterator = 0
        VectorX = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_270 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_279 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_290 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_305 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        for key in self.DictionaryWithRotationalDOFsRemoved:
            if key == 0:
                continue
            else:
                Temp = self.DictionaryWithRotationalDOFsRemoved[key]
                VectorX[iterator] = key
                VectorY_270[iterator] = 20*mt.log10(np.abs(Temp[5][2])/ReferenceAcceleration)
                VectorY_279[iterator] = 20*mt.log10(np.abs(Temp[8][2])/ReferenceAcceleration)
                VectorY_290[iterator] = 20*mt.log10(np.abs(Temp[11][2])/ReferenceAcceleration)
                VectorY_305[iterator] = 20*mt.log10(np.abs(Temp[14][2])/ReferenceAcceleration)
                iterator = iterator + 1
        plt.figure(1)
        plt.plot(VectorX,VectorY_270,'ro',label = '270')
        plt.plot(VectorX,VectorY_279,'y--',label = '279')
        plt.plot(VectorX,VectorY_290,'bs',label = '290')
        plt.plot(VectorX,VectorY_305,'g^',label = '305')
        plt.xlabel('Frequency [Hz]')
        plt.ylabel('Accelerance (a_z_Node#/F_z_Node103) - Magnitude [dB]')
        plt.legend()
        plt.title('FRFs of uncoupled Structure 2')
        pylab.xlim([0,1000])
        pylab.ylim([100,260])
        pylab.savefig('FRF_Uncoupled_Structure2.png', dpi=600)
        
    def PlottingIndividualFRFsForStructure1(self):
        ReferenceAcceleration = 0.00000134
        iterator = 0
        VectorX = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_452 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_464 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_483 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        VectorY_498 = np.zeros(len(self.DictionaryWithRotationalDOFsRemoved) - 1)
        for key in self.DictionaryWithRotationalDOFsRemoved:
            if key == 0:
                continue
            else:
                Temp = self.DictionaryWithRotationalDOFsRemoved[key]
                VectorX[iterator] = key
                VectorY_452[iterator] = 20*mt.log10(np.abs(Temp[14][2])/ReferenceAcceleration)
                VectorY_464[iterator] = 20*mt.log10(np.abs(Temp[14][5])/ReferenceAcceleration)
                VectorY_483[iterator] = 20*mt.log10(np.abs(Temp[14][8])/ReferenceAcceleration)
                VectorY_498[iterator] = 20*mt.log10(np.abs(Temp[14][11])/ReferenceAcceleration)
                iterator = iterator + 1
        plt.figure(2)
        plt.plot(VectorX,VectorY_452,'ro',label = 'Node452')
        plt.plot(VectorX,VectorY_464,'y--',label = 'Node464')
        plt.plot(VectorX,VectorY_483,'bs',label = 'Node483')
        plt.plot(VectorX,VectorY_498,'g^',label = 'Node498')
        plt.xlabel('Frequency [Hz]')
        plt.ylabel('Accelerance (a_z_Node687/F_z_Node#) - Magnitude [dB]')
        plt.legend()
        plt.title('FRFs of uncoupled Structure 1')
        pylab.xlim([0,1000])
        pylab.ylim([100,260])
        pylab.savefig('FRF_Uncoupled_Structure1.png', dpi=600)
        
    
        
        