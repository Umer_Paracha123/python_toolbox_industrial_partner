# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 19:17:36 2017

@author: Yilun Sun
"""

"""
class to define a unit
"""
class Unit(object):
    """
    Static properties for the class
    """
    _BaseUnitNames = ["Length","Mass","Time","Electric Current","Temperature",\
                       "Amount of substance","Luminous Intensity","Angle"]
    _BaseUnits = ["m","kg","s","A","K","mol","cd","rad"]
    

    def __init__(self,DimVector,Symbol,SIFactor,dBref,Name):
        """
        Constructor of the class
        """
        self.DimVector = DimVector
        self.Symbol = Symbol
        self.SIFactor = SIFactor
        self.dBref = dBref
        self.Name = Name
        
    def label(self):
        """
        This method is used to show the symbol of the unit
        """
        return self.Symbol
    
    @classmethod
    def Defaults(cls):
        """
        This method is used to get a list of predefined units which can be used 
        directly
        """
        lDf = []
        DimVector = [0]*len(cls._BaseUnits)
        for i in range(len(cls._BaseUnits)):
            DVCopy = DimVector[:]
            DVCopy[i] = 1
            lDf.append(Unit(DVCopy,cls._BaseUnits[i],(1,0),1,cls._BaseUnitNames[i]))
        lDf.append(Unit(DimVector,".",(1,0),1,"Dimensionless"))
        lDf.append(Unit([1,0,-2,0,0,0,0,0],"m/s^2",(1,0),1,"Acceleration"))
        return lDf
    
    @classmethod
    def fromSymbol(cls,Symbol):
        """
        This method is used to get a unit object by giving a symbol as argument
        """
        lDf = cls.Defaults()
        for oUnit in lDf:
            if oUnit.label() == Symbol:
                return oUnit
        print("Error: no unit in the Defaults has this symbol!")
        