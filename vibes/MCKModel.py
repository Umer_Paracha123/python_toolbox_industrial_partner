# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 17:39:40 2017

@author: Yilun Sun
"""
from vibes.FRF import FRF
from vibes.ModalModel import ModalModel
import numpy as np

class MCKModel(object):
    
    def __init__(self,Name,M,C,K,DoF_Set):
        """
        Constructor for the MCK Model object
        """
        self.Name = Name
        self.MassMatrix = M
        self.StiffnessMatrix = K
        self.DampingMatrix = C
        self.DoF_Set = DoF_Set
        
    # Calculates mass of system by adding all the entries of mass matrix
    def calMass(self):
        mass = 0.0
        size = len(self.MassMatrix)
        for i in range (0,size):
            for j in range (0,size):
                mass = mass + self.MassMatrix[i][j]
        return mass
    
    # Calculates Y matrix for subsequent calculation of FRF model from MCK model.
    def toYMatrix(self,Freq):
        """
        this function is used to generate a Y matrix of a given frequency
        """
        ZMatrix = -Freq*Freq*self.MassMatrix + Freq*1j*self.DampingMatrix + self.StiffnessMatrix
        return np.linalg.inv(ZMatrix)
    
    # Calculates FRF model from MCK Model.
    def toFRF(self,Freqs):
        """
        this function is used to vonvert a MCK Model to a FRF object
        """
        oFRF = FRF()
        oFRF.Name = self.Name
        oFRF.Freqs = Freqs
        for Freq in Freqs:
            oFRF.Data[Freq] = self.toYMatrix(Freq)
        return oFRF
    
    #Finds eigen modes and eigen frequencies of given system. Hence calculates modal model from MCK Model.
    def toModalModel(self):
        oModalModel = ModalModel()
        oModalModel.Name = self.Name
        oModalModel.calculatemoddalmodel(self)
        return oModalModel
    
        #Adds Rayleigh Damping to the system
    def CalculateRayleighDamping(self, oModalModel):
        #This is initialized as array of order 2 x 2. Hence it can be called as [][] or [,] whereas if it were initialized as np.matrix([[1,0]][0,1]] then you cannot use [][] but we use [0,0] etc.
        #Let A = np.matrix([[1,2],[2,3]]). Then we must use Rayleighconstant[0][0]*A. Cannot skip second indice in Rayleighconstant
        Matrixoffrequencies = np.zeros([2,2],complex) 
        Dampingratio = np.zeros([2,1],complex)
#        Rayleighconstants = np.zeros([2,1],complex)
        Rayleighconstants = np.zeros([2,1],complex)
        Matrixoffrequencies [0][0] = 1/oModalModel.frequency[0]
        Matrixoffrequencies [1][0] = 1/oModalModel.frequency[1]
        Matrixoffrequencies [0][1] = oModalModel.frequency[0]
        Matrixoffrequencies [1][1] = oModalModel.frequency[1]
        Dampingratio[0] = oModalModel.dampingratio[0]
        Dampingratio[1] = oModalModel.dampingratio[1]
        if (np.linalg.det(Matrixoffrequencies/2) == 0):
            print('Cannot invert matrix to find Rayleigh constants. Consequently, the Damping ratio of first two eigen frequencies is same hence Rayleigh constants will be equal')
            Rayleighconstants[1] = 2*Dampingratio[0]/(Matrixoffrequencies [0][1]+ Matrixoffrequencies [1][1])
            Rayleighconstants[0] = Matrixoffrequencies [0][1]*Matrixoffrequencies [1][1]*Rayleighconstants[1]
        else:
            Rayleighconstants = np.dot(Dampingratio,np.linalg.inv(Matrixoffrequencies/2))
        RayleighDamping = Rayleighconstants[0]*self.MassMatrix + Rayleighconstants[1]*self.StiffnessMatrix
        print('The Rayleigh damping is:', RayleighDamping)