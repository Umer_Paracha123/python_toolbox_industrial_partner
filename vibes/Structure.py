# -*- coding: utf-8 -*-
"""
Created on Thu Dec 21 13:46:27 2017

@author: ga83wec
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pylab
import math as mt

class Structure(object):
     
    def __init__(self,Name):
        self.Name = Name
        self.FinalCoupledFRFMatrix = {}
            
    def CoupleStructures(self,oSubStrA,oSubStrB):
        self.strA = oSubStrA
        self.strB = oSubStrB   
#        x = oSubStrA.System_Dynamics["hammer"].DictionaryWithRotationalDOFsRemoved[1]    
#        ColoumnNumber1 = len(x[0])
#        y = oSubStrB.System_Dynamics["hammer"].DictionaryWithRotationalDOFsRemoved[1]
#        ColoumnNumber2 = len(y[0])
#        ColoumnNumber = min(ColoumnNumber1,ColoumnNumber2)
        FRF_Combined = {}
        MatrixB = {}
        for key in oSubStrA.System_Dynamics["hammer"].DictionaryWithRotationalDOFsRemoved: #!!!!! Assumption: Both structures are excited at same frequenices.
            if key == 0.0:
                continue 
            else:    
                FRF_Combined[key] = self.CalculateCombinedFRFMatrixofBothStructures(key) 
                MatrixB[key] = self.CalculateBooleanMatrixB(12,len(FRF_Combined[key]))
                self.FinalCoupledFRFMatrix[key] = self.FinalCoupledFRFMatrixForBothStructures(FRF_Combined[key],MatrixB[key]) #The final "global" coupled FRF matrix
        return self.FinalCoupledFRFMatrix, FRF_Combined, MatrixB
     
        
    def CalculateBooleanMatrixB(self,RowNumber,ColoumnNumber):
        j = 0
        MatrixB = np.zeros([RowNumber,ColoumnNumber],int)
        for i in range (0,RowNumber):
            MatrixB[i][j+3] = -1
            MatrixB[i][j+15] = 1 #!!!!!! Original 12
            j = j + 1
        return MatrixB
         
    
    def CalculateCombinedFRFMatrixofBothStructures(self,key):    
        CoupledFRF1 = self.strA.System_Dynamics["hammer"].DictionaryWithRotationalDOFsRemoved[key]
        CoupledFRF2 = self.strB.System_Dynamics["hammer"].DictionaryWithRotationalDOFsRemoved[key]
        RowNumber = len(CoupledFRF1) + len(CoupledFRF2)
        ColoumnNumber = len(CoupledFRF1[0]) + len(CoupledFRF2[0])
        CombinedFRFMatrix = np.zeros([RowNumber,ColoumnNumber],complex)
        CombinedFRFMatrix[0:len(CoupledFRF2),0:len(CoupledFRF2[0])] = CoupledFRF2
        CombinedFRFMatrix[RowNumber - len(CoupledFRF1):RowNumber,ColoumnNumber - len(CoupledFRF1[0]):ColoumnNumber] = CoupledFRF1
        return CombinedFRFMatrix
    
    def FinalCoupledFRFMatrixForBothStructures(self,FRF_Combined,MatrixB):

# This part calculates the complete global coupled FRF matrix while the second part calculates the first entry of the matrix.

        SecondTerm = np.dot(FRF_Combined,np.transpose(MatrixB))
        ThirdTermWithoutInverse = np.dot(np.dot(MatrixB,FRF_Combined),np.transpose(MatrixB))
        ThirdTermInverted = np.linalg.inv(ThirdTermWithoutInverse)
        FourthTerm = np.dot(MatrixB,FRF_Combined)
        SecondTermFinal = np.dot(np.dot(SecondTerm,ThirdTermInverted),FourthTerm)
        FinalCoupledFRFMatrix = FRF_Combined - SecondTermFinal
           
        return FinalCoupledFRFMatrix
    
    def PlottingFunctionForDualCoupling(self):
        ReferenceAcceleration = 0.00000134               
        iterator = 0
        VectorX = np.zeros(len(self.FinalCoupledFRFMatrix) + 1)
        VectorY = np.zeros(len(self.FinalCoupledFRFMatrix) + 1)
        for key in self.FinalCoupledFRFMatrix:
            Temp = self.FinalCoupledFRFMatrix[key]
            
            VectorX[iterator + 1] = key
            VectorY[iterator + 1] = 20*mt.log10(np.abs(Temp[29][2])/ReferenceAcceleration)
#            VectorY[iterator + 1] = 20*mt.log10(np.abs(Temp[3][0])/ReferenceAcceleration) 
            
#            VectorY[iterator] = np.abs(Temp[29][2])
            iterator = iterator + 1
        
        plt.figure(3)
        plt.title('FRFs of coupled Structure 1 & 2')
        plt.plot(VectorX,VectorY,label = 'LMFBS Coupling (Python)')
        plt.xlabel('Frequency [Hz]')
        plt.ylabel('Accelerance (a_z_Node687/F_z_Node103) - Magnitude [dB]')
        plt.legend()
        pylab.xlim([0,1000])
        pylab.ylim([100,260])                                      
        pylab.savefig('Coupled_FRF_Python.png', dpi=600)         

        return VectorY         
        
    def PlottingFunctionForFiniteElementsData(self,FileName):
        ReferenceAcceleration =  0.00000134              
        
        df = pd.read_csv(FileName)
        Length = len(df.iloc[3])
        VectorX = df['Freq(Hz)']
        VectorY_AccX = df.iloc[:,Length - 14]/ReferenceAcceleration
        VectorY_AccY = df.iloc[:,Length - 12]/ReferenceAcceleration
        VectorY_AccZ = df.iloc[:,Length - 10]/ReferenceAcceleration
        
        for iterator in range (1,len(VectorX)):
            VectorY_AccX[iterator] = 20*mt.log10(VectorY_AccX[iterator])
            VectorY_AccY[iterator] = 20*mt.log10(VectorY_AccY[iterator])
            VectorY_AccZ[iterator] = 20*mt.log10(VectorY_AccZ[iterator])
               
        plt.plot(VectorX,VectorY_AccZ, 'r--', label = 'Validation (Nastran)')
        plt.legend()
        pylab.xlim([0,1000])
        pylab.ylim([100,260])    
        pylab.savefig('Coupled_FRF_NastranAndPython.png', dpi=600)
        
       
        plt.figure(4)
        plt.title('FRFs of coupled Structure 1 & 2 in Nastran')
        plt.plot(VectorX,VectorY_AccZ,label = 'Validation (Nastran)')
        plt.legend()
        pylab.xlim([0,1000])
        pylab.ylim([100,260])
        pylab.savefig('Coupled_FRF_Nastran.png', dpi=600)
        
        
    def PlottingExperimentalData(self,FileName):        #Doesn't match with the other data --> ToFix
        ReferenceAcceleration =  0.00000134  
        
        df = pd.read_table(FileName)
        VectorX = df.iloc[:,0]
        VectorY = df.iloc[:,1]/ReferenceAcceleration
        
        for iterator in range (0,len(VectorX)):
            VectorY[iterator] = 20*mt.log10(VectorY[iterator])
        
        plt.plot(VectorX,VectorY,label = 'Experimental')
        plt.legend()
        pylab.xlim([0,1000])
        pylab.ylim([100,260])
        pylab.savefig('Coupled_FRF_Experimental.png', dpi=600)
        
        
            
        
        