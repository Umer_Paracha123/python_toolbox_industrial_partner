# -*- coding: utf-8 -*-
"""
Created on Wed Dec 20 13:36:37 2017

@author: ga83wec
"""


import pandas as pd
import numpy as np
import math as mt

df = pd.read_csv('model1_s2_1.csv')

DofsPerNode = 6
NumberofNodesToBeCoupled = 4
NumbeOfReferencesPerNode = 3
NumbeofExcitationPoints = 1
NumberofStructuresToBeCoupled = 2
FrequencyofInterest = 1

TranspassedEntries = 0
RowNumber = DofsPerNode * NumberofNodesToBeCoupled
ColoumnNumber = NumbeOfReferencesPerNode * (NumberofNodesToBeCoupled + NumbeofExcitationPoints)

FRFMatrix = np.zeros([RowNumber,ColoumnNumber],complex)

SelectedRow = df.iloc[FrequencyofInterest]

for j in range (0,ColoumnNumber):
    for i in range (0,RowNumber):
      Magnitude = SelectedRow.iloc[1 + 2*i + TranspassedEntries]
      PhaseinRadians = mt.radians(SelectedRow.iloc[2 + 2*i + TranspassedEntries])
      RealPart = Magnitude * mt.cos(PhaseinRadians)
      ImaginaryPart = Magnitude * mt.sin(PhaseinRadians)
      FRFMatrix[i][j] = complex(RealPart,ImaginaryPart)
    TranspassedEntries = TranspassedEntries + 2*(RowNumber)


