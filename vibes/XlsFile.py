# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 17:18:39 2017

@author: Yilun Sun
"""

import openpyxl as opx
import numpy as np
from vibes.Unit import Unit
from vibes.Quantity import Quantity
from vibes.ui.Sensor import Sensor
from vibes.DoF import DoF
from vibes.math import math

"""
This class is used to generatea an object which represents an xls file and can realize 
the import and export function between the xls file and DoF(sensor or impact) objects
"""
class XlsFile(object):
    def __init__(self,FilenameXls):
        """
        Constructor of the class and taking the xls file name as input
        """
        self._FilenameXls = FilenameXls
        
    def setFilename(self,FilenameXls):
        """
        Used to set the xls file name
        """
        self._FilenameXls = FilenameXls
        
    def read(self,NameObj):
        """
        Used to generate a list of DoF(sensor or impact) objects from 
        the xls file
        """
        try:
            XFile = opx.load_workbook(self._FilenameXls)
            ws = XFile[NameObj + "s"]
        except:
            raise Exception("Error: enter a proper name of output objects(Sensor, Impact or Channel)")
        objs = []
        dNum2Prop = eval(NameObj).Num2Prop

        for i in range(2,ws.max_row+1):
            obj = eval(NameObj)()
            for prop in dNum2Prop:
                if type(dNum2Prop[prop]).__name__ == "int":
                    val = ws.cell(row=i,column=dNum2Prop[prop]).value
                else:
                    lnum = dNum2Prop[prop]
                    val = []
                    for num in lnum:
                        val.append(ws.cell(row=i,column=num).value)
                setattr(obj,prop,val)
            obj.Quantity = Quantity(obj.Quantity,obj.Unit)
            obj.Unit = Unit.fromSymbol(obj.Unit)
            obj.Position = math.list2matrix(obj.Position,3,1,False)
            if hasattr(obj,"Orientation") == True:
                obj.Orientation = math.list2matrix(obj.Orientation,3,3,False)
            if hasattr(obj,"Direction") == True:
                obj.Direction = math.list2matrix(obj.Direction,3,1,False)
            objs.append(obj)
        return objs
    
    def write(self,objs):
        """
        Used to write a list of DoF(sensor or impact) objects into 
        the xls file
        """
        XFile = opx.Workbook()
        for obj in objs:
            NameObj = type(obj).__name__
            dNum2Prop = eval(NameObj).Num2Prop
            sheetname = NameObj + "s"
            if not sheetname in XFile.sheetnames:
                XFile.create_sheet(sheetname,0)
                ws = XFile[sheetname]
                for prop in dNum2Prop:
                    if type(dNum2Prop[prop]).__name__ == "int":
                        ws.cell(row=1,column=dNum2Prop[prop]).value = obj.Name
            else:
                ws = XFile[sheetname]
            ws.cell(row=ws.max_row + 1,column=1).value = obj.Name
        XFile.save(self._FilenameXls)

