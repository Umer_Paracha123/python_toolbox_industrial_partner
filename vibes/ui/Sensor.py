# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 01:26:56 2017

@author: Yilun Sun
"""
import openpyxl as opx
import numpy as np
from vibes.Unit import Unit
from vibes.Quantity import Quantity
from vibes.DoF import DoF

"""
Class to generate a sensor object
"""
class Sensor(object):
    """
    Static property for the use in XlsFile class
    """
    Num2Prop = {"Name":1,"Description":2,"Type":3,"Size":4,"NodeNumber":5,\
                  "Grouping":6,"Quantity":7,"Unit":8,"Position":[9,10,11],\
                  "Orientation":[12,13,14,15,16,17,18,19,20]}
    def __init__(self):
        """
        Default constructor for the sensor object
        """
        self.Description = None
        self.Alignment = None
        self.Name = None
        self.Grouping = None
        self.NodeNumber = None
        self.Position = None
        self._e = None
        self._grp = None
        self._nr = None
        self._p = None
        self.Orientation = None
        self.Size = None
        self.Type = None
        self.Quantity = None
        self.Unit = None
        
    @staticmethod
    def toDoF(sensors):
        """
        This static method is used to convert a list of sensor objects into 
        channel objects
        """
        DoFs = []
        for sr in sensors:
            for i in range(3):
                oDoF = DoF()
                oDoF.Description = sr.Name + " " + chr(88+i)
                oDoF.NodeNumber = sr.NodeNumber
                oDoF.Grouping = sr.Grouping
                oDoF.Quantity = sr.Quantity
                oDoF.Unit = sr.Unit
                oDoF.Position = sr.Position
                oDoF.Direction = np.matrix(sr.Orientation[i]).transpose()
                DoFs.append(oDoF)
        return DoFs

            
    
        