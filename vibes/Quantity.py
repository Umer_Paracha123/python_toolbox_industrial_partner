# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 21:24:12 2017

@author: Yilun Sun
"""

"""
Class to generate a quantity object
"""
class Quantity(object):
    def __init__(self,Name,Symbol):
        """
        Constructor for the quantity object
        """
        self.Name = Name
        self.Unit = Symbol
        
    def label(self):
        """
        This method is used to show the property of the quantity object
        """
        slabel = self.Name + " (" + self.Unit + ")"
        return slabel
    
    @staticmethod
    def fromUnit(oUnit):
        """
        This static method is used to generate a quantity object from a unit
        object
        """
        oQ = Quantity(oUnit.Name,oUnit.Symbol)
        return oQ