# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 16:40:21 2017

@author: Yilun Sun
"""

class SubStructure(object):
    def __init__(self,Name):
        """
        Constructor for the Structure object
        """
        self.Name = Name
        self.System_Dynamics = {}
#        self.DoF_List = DoFs # DoF objects from XlsFile.read function
        
    def addFRF(self,oFRF):
        """
        this function is used to add FRF objects to the structure object
        """
        if oFRF.Name in self.System_Dynamics:
            rep = input("The given FRF matrix already existed. \
                        Do you want to replace it? [y]es or [n]o")
            if rep == "y":
                self.System_Dynamics[oFRF.Name] = oFRF
        else:
            self.System_Dynamics[oFRF.Name] = oFRF