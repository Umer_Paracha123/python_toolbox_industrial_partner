# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 20:39:03 2017

@author: John
"""

import numpy as np

class math():
    @staticmethod
    def list2matrix(listin,nrow,ncolumn,browfirst):
        if nrow*ncolumn == len(listin):
            a =[]
            for j in range(nrow):
                b = []
                for k in range(ncolumn):
                    b.append(0.0)
                a.append(b)
            matrixout = np.matrix(a)
#            print(matrixout)
            for i in range(len(listin)):
                if browfirst == True:
                    matrixout[i//ncolumn,i%ncolumn] = listin[i]
                else:
                    matrixout[i%nrow,i//nrow] = listin[i]
            return matrixout
        else:
            raise Exception("The given list doesn't coincide with \
                            the row and column number!")