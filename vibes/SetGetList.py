# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 15:12:20 2017

@author: Yilun Sun
"""

"""
class for a series of methods to deal with list
Motivation: since the methods below will always be used by a list of objects, they 
            are now extracted from those classes to form a collection.
"""
class SetGetList():
    @staticmethod
    def find(objs,propname,lpropval, condition = "=="):
        """
        This method is used to find the index of the objects in a list whose property 
        values fulfill the given conditions
        """
        index = []
        toappend = False
        for i in range(len(objs)):
            if condition == "==":       
                for propval in lpropval:
                    if getattr(objs[i],propname) == propval:
                        toappend = True
                        break
            elif condition == "!=":
                for propval in lpropval:
                    if getattr(objs[i],propname) != propval:
                        toappend = True
                    else:
                        toappend = False
                        break                
            if toappend == True:
                index.append(i)
                toappend = False
        return index
    
    @staticmethod
    def select(objs,propname,lpropval, condition = "=="):
        """
        This method is used to find the objects in a list whose property 
        values fulfill the given conditions
        """
        s_objs = []
        toappend = False
        for obj in objs:
            if condition == "==":       
                for propval in lpropval:
                    if getattr(obj,propname) == propval:
                        toappend = True
                        break
            elif condition == "!=":
                for propval in lpropval:
                    if getattr(obj,propname) != propval:
                        toappend = True
                    else:
                        toappend = False
                        break                
            if toappend == True:
                s_objs.append(obj)
                toappend = False
        return s_objs
    
    @staticmethod
    def setList(objs,propname,propval):
        """
        This method is used to set the property values of the given objects in a list
        """
        for obj in objs:
            setattr(obj, propname, propval)
    
    @staticmethod       
    def getList(objs,propname):
        """
        This method is used to get the property values of the given objects in a list
        """
        lprop = []
        for obj in objs:
            lprop.append(getattr(obj,propname))
        return lprop