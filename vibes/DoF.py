# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 18:37:28 2017

@author: Yilun Sun
"""

import openpyxl as opx
import numpy as np

"""
class for channel
"""
class DoF(object):
    CountDoF = 0 #static property for the number of DoFs in the running time
    """
    Static property for the use in XlsFile class
    """
    Num2Prop = {"Name":1,"Description":2,"NodeNumber":3,\
                  "Grouping":4,"Quantity":5,"Unit":6,"Position":[7,8,9],\
                  "Direction":[10,11,12]}
    def __init__(self):
        """
        Default constructor for the channel object
        """
        self.Description = None
        self.Alignment = None
        
        self.Grouping = None
        self.NodeNumber = None
        self.Position = np.matrix([[0],[0],[0]])
        self.Direction = np.matrix([[0],[0],[0]])
        self.Size = None
        self.Type = None
        self.Quantity = None
        self.Unit = None
        self.Notes = ""
        self.__class__.CountDoF += 1 #automatically add 1 when generating a new object
        self.Index = self.__class__.CountDoF
        self.Name = "DoF " + str(self.Index)
        
