clear all
close all


RotationalDOFsRemoved1 = load('RotationalDOFsRemoved1_matlab');

Freq = 1:1000;
nFreq = 1000;
df = 1;
T = 1;
BW = 999;

Data = struct2cell(RotationalDOFsRemoved1);


test=zeros(15,12,length(Data));
    
for i=1:length(Data)
    test(:,:,1)=Data{i};
end

