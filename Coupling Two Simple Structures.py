# -*- coding: utf-8 -*-
"""
Created on Thu Dec 28 09:59:10 2017

@author: ga83wec
"""
import vibes
from vibes.FRF import FRF
#import scipy.io
#import traceback
#import warnings
#import sys
#
#
#def warn_with_traceback(message, category, filename, lineno, file=None, line=None):
#
#    log = file if hasattr(file,'write') else sys.stderr
#    traceback.print_stack(file=log)
#    log.write(warnings.formatwarning(message, category, filename, lineno, line))
#
#warnings.showwarning = warn_with_traceback


#------ Reading FRF data from file and converting it into FRF matrices

oFRF_s2 = FRF()
oFRF_s2.Name = "hammer"
oFRF_s2.FillData(6,4,3,1,2)
FRFMatrix2 = oFRF_s2.ReadDataAndCreateFRFMatricesS2('model1_s2_1.csv','model1_s2_dfrf_excitation_response_point_for_check.csv',True)
#Dictionary2 = oFRF_s2.ReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection(True)
RotationalDOFsRemoved2 = oFRF_s2.RotationalDOFsRemoved(3,4,True)
#DictionaryWithRotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken2 = oFRF_s2.RotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken(True)
oFRF_s2.PlottingIndividualFRFsForStructure2()

oFRF_s1 = FRF()
oFRF_s1.FillData(6,4,3,1,2)
oFRF_s1.Name = "hammer"
FRFMatrix1 = oFRF_s1.ReadDataAndCreateFRFMatricesS1('model1_s1_1.csv','model1_s1_dfrf_excitation_response_point_for_check.csv',False)
#Dictionary1 = oFRF_s1.ReadingExcitationPointFileAndResponsePointFileButOnlyInZDirection(False)
RotationalDOFsRemoved1 = oFRF_s1.RotationalDOFsRemoved(3,4,False)
#DictionaryWithRotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken1 = oFRF_s1.RotationalDOFsRemovedAndOnlyOneCouplingNodeIsTaken(False)
oFRF_s1.PlottingIndividualFRFsForStructure1()


#------ Creating Sub structures

oSubStrA = vibes.SubStructure("A") # generate a structure with the name A
oSubStrA.addFRF(oFRF_s1) # add the FRF object to the structure

oSubStrB = vibes.SubStructure("B") # generate a structure with the name B
oSubStrB.addFRF(oFRF_s2) # add the FRF object to the structure

#------ Coupled Structure

oStrC = vibes.Structure("C")
FinalCoupledFRFMatrix, FRF_Combined, MatrixB = oStrC.CoupleStructures(oSubStrA,oSubStrB)
VectorY = oStrC.PlottingFunctionForDualCoupling()
oStrC.PlottingFunctionForFiniteElementsData('model1_all_1.csv')
#oStrC.PlottingExperimentalData('FBS_model_response_Z.txt')


#scipy.io.savemat('Two',RotationalDOFsRemoved2)