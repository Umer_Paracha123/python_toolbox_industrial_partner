from PyQt5 import QtCore, QtGui

class MyClass(object):
    def __init__(self, arg):
        super(MyClass, self).__init__()
        self.arg = arg

class myWindow(QtGui.QtWidgets):
    def __init__(self, parent=None):
        super(myWindow, self).__init__(parent)

        self.comboBox = QtGui.QComboBox(self)
        self.comboBox.addItems([str(x) for x in range(3)])

        self.myObject=MyClass(id(self) )

        slotLambda = lambda: self.indexChanged_lambda(self.myObject)
        self.comboBox.currentIndexChanged.connect(slotLambda)

    @QtCore.pyqtSlot(str)
    def indexChanged_lambda(self, string):
        print ('lambda:', type(string), string)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('myApp')
    dialog = myWindow()
    dialog.show()
    sys.exit(app.exec_())