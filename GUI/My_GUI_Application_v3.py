# @author: Sayantini Majumdar

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import *
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
import sys
import GUI_designer

class MainApplication(QMainWindow, GUI_designer.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainApplication, self).__init__(parent)
        self.setupUi(self)

        self.menu=['--Please select--','Uncoupled','Coupled']

        self.comboBox.addItems(self.menu)
        self.y_checkboxes_list = []  ###keeps a list of all FRFs to be plotted
        self.flag_checked = {} ### keeps a record of all current excitation and response DOFs which have been checked by the user


    @QtCore.pyqtSlot(str)
    def on_comboBox_currentIndexChanged(self,text):
        self.structureComboBox1.clear()
        items = ['PT_A', 'FV_B']


        if text=='--Please select--':
            self.structureComboBox1.addItem('--Please select--')
            self.structureComboBox2.addItem('--Please select--')
            self.excitationPointComboBox.addItem('--Please select--')
            self.responsePointComboBox.addItem('--Please select--')
            self.structureComboBox2.setEnabled(True)

        elif text=='Uncoupled':
            self.PTstructureLabel.setText("Choose Structure")
            self.FVstructureLabel.setText("Choose Structure")
            self.structureComboBox1.clear()
            self.structureComboBox2.clear()
            self.structureComboBox1.addItem('--Please select--')
            self.structureComboBox1.addItems(items)
            self.structureComboBox2.setEnabled(False)
            self.FVstructureLabel.setStyleSheet("background-color:gray")

        elif text=='Coupled':
            coupledRespItems = ['N901', 'N905', 'N907', 'N7000002', 'N40030267', 'N40030315', 'N40071803', 'N40088143']
            self.FVstructureLabel.setStyleSheet("")
            self.FVstructureLabel.setText("Choose FV substructure")
            self.structureComboBox1.clear()
            self.structureComboBox1.addItem('--Please select--')
            self.structureComboBox1.addItem('PT_A')
            self.structureComboBox1.addItem('PT_C')
            self.PTstructureLabel.setText("Choose PT substructure")
            self.structureComboBox2.setEnabled(True)
            self.structureComboBox2.clear()
            self.structureComboBox2.addItem('--Please select--')
            self.structureComboBox2.addItem('FV_B')
            self.structureComboBox2.addItem('FV_D')
            self.excitationPointComboBox.clear()
            self.excitationPointComboBox.addItem('--Please select--')
            self.excitationPointComboBox.addItem('N8400001')
            self.responsePointComboBox.clear()
            self.responsePointComboBox.addItem('--Please select--')
            self.responsePointComboBox.addItems(coupledRespItems)



    @QtCore.pyqtSlot(str)
    def on_structureComboBox1_currentIndexChanged(self,text):
        if self.comboBox.currentText() == 'Uncoupled':
            responsePointItemsPT_A = ['N100', 'N110', 'N300', 'N310', 'N331', 'N332', 'N333', 'N400', 'N410', 'N431', 'N432',
                              'N433', 'N8060', 'N509000', 'N509031', 'N7823200']
            excitationPointItems = ['N100', 'N110', 'N300', 'N310', 'N331', 'N332', 'N333', 'N400', 'N410', 'N431', 'N432',
                                'N433', 'N8060', 'N509000', 'N509031', 'N7823200']
            responsePointItemsFV_B = ['N901', 'N905', 'N907', 'N7000002', 'N40030267', 'N40030315', 'N40071803', 'N40088143']
            if text == '--Please select--':
                self.excitationPointComboBox.clear()
                self.excitationPointComboBox.addItem('--Please select--')
                self.responsePointComboBox.clear()
                self.responsePointComboBox.addItem('--Please select--')
            elif text=='PT_A':
                self.excitationPointComboBox.clear()
                self.excitationPointComboBox.addItem('--Please select--')
                self.excitationPointComboBox.addItem('N8400001')
                self.responsePointComboBox.clear()
                self.responsePointComboBox.addItem('--Please select--')
                self.responsePointComboBox.addItems(responsePointItemsPT_A)
            elif text=='FV_B':
                self.excitationPointComboBox.clear()
                self.excitationPointComboBox.addItem('--Please select--')
                self.excitationPointComboBox.addItems(excitationPointItems)
                self.responsePointComboBox.clear()
                self.responsePointComboBox.addItem('--Please select--')
                self.responsePointComboBox.addItems(responsePointItemsFV_B)




    @QtCore.pyqtSlot()
    def on_toWorkspaceButton_clicked(self):
        ### remove any previous widgets from the workspace
        for i in reversed(range(self.verticalLayout.count())):
            widgetToRemove = self.verticalLayout.itemAt(i).widget()
            # remove it from the layout list
            self.verticalLayout.removeWidget(widgetToRemove)
            # remove it from the gui
            widgetToRemove.setParent(None)

        self.y_checkboxes_list = [] ### resets value of the list
        flag_checked = {'ex_x':0, 'ex_y':0, 'ex_z':0, 'res_x':0, 'res_y':0, 'res_z':0, 'res_rx':0, 'res_ry':0, 'res_rz':0}
        if self.ex_xcheckBox.isChecked():
            flag_checked['ex_x'] = 1
        if self.ex_ycheckBox.isChecked():
            flag_checked['ex_y'] = 1
        if self.ex_zcheckBox.isChecked():
            flag_checked['ex_z'] = 1
        if self.res_xcheckBox.isChecked():
            flag_checked['res_x'] = 1
        if self.res_ycheckBox.isChecked():
            flag_checked['res_y'] = 1
        if self.res_zcheckBox.isChecked():
            flag_checked['res_z'] = 1
        if self.res_rxcheckBox.isChecked():
            flag_checked['res_rx'] = 1
        if self.res_rycheckBox.isChecked():
            flag_checked['res_ry'] = 1
        if self.res_rzcheckBox.isChecked():
            flag_checked['res_rz'] = 1

        excitationDOFs = []  ### captures the current excitation DOFs selected by the user
        responseDOFS = []  ### captures the current response DOFs selected by the user
        keys_checked = [dofs for dofs in flag_checked if flag_checked[dofs] == 1]  ### list comprehension for the keys correcponding to the checked value 1
        ### extract ex and res DOFs and store them in separate lists
        for i in range(len(keys_checked)):
            if keys_checked[i].startswith('ex'):
                excitationDOFs.append(keys_checked[i])
            else:
                responseDOFS.append(keys_checked[i])
        if not excitationDOFs or not responseDOFS or self.excitationPointComboBox.currentText() == '--Please select--' or self.responsePointComboBox.currentText() == '--Please select--':
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setText("At least one input is missing. Please check and try again.")
            msgBox.setWindowTitle("Missing input parameters")
            msgBox.exec()
        elif self.comboBox.currentText() == 'Uncoupled':
            if self.structureComboBox1.currentText() != '--Please select--':
                ex_node = self.excitationPointComboBox.currentText()
                res_node = self.responsePointComboBox.currentText()
                y_checkBox_text = 'Y_' + self.structureComboBox1.currentText() + '_' + res_node
                #print(keys_checked)
                ### get check box texts with combinations of ex and res DOFs and store them in class variable self.y_checkboxes_list
                for item1 in excitationDOFs:
                    for item2 in responseDOFS:
                        y_checkBox_text += '_' + item2 + '_' + ex_node + '_' + item1
                        self.y_checkboxes_list.append(y_checkBox_text)
                        y_checkBox_text = 'Y_' + self.structureComboBox1.currentText() + '_' + res_node
                for i in self.y_checkboxes_list:
                    pCheckBox = QtWidgets.QCheckBox()
                    pCheckBox.setText(i)
                    self.verticalLayout.addWidget(pCheckBox)


        elif self.comboBox.currentText() == 'Coupled':
            ex_node = self.excitationPointComboBox.currentText()
            res_node = self.responsePointComboBox.currentText()
            s1 = self.structureComboBox1.currentText()
            s2 = self.structureComboBox2.currentText()
            y_checkBox_text = 'Y_' + s1 + '_' + s2 + '_' + res_node
            ### get check box texts with combinations of ex and res DOFs and store them in class variable self.y_checkboxes_list
            for item1 in excitationDOFs:
                for item2 in responseDOFS:
                    y_checkBox_text += '_' + item2 + '_' + ex_node + '_' + item1
                    self.y_checkboxes_list.append(y_checkBox_text)
                    y_checkBox_text = 'Y_' + s1 + s2 + '_' + res_node

            for i in self.y_checkboxes_list:
                pCheckBox = QtWidgets.QCheckBox()
                pCheckBox.setText(i)
                self.verticalLayout.addWidget(pCheckBox)

    @QtCore.pyqtSlot()
    def on_plotSelectionButton_clicked(self):
        ### first check for empty workspace
        # if self.verticalLayout.itemAt(0).widget() is None:
        #     print('Nothing to do. Workspace empty.')

        ### then check if any check box is selected
        count =0  ### keeps a count of the number of choices given by the user to be ultmately plotted
        for i in reversed(range(self.verticalLayout.count())):
            w = self.verticalLayout.itemAt(i).widget()
            if not w.isChecked():
                flag = 1
            else:
                flag = 0
                count += 1
        if flag:
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Information)
            msgBox.setText("No items have been selected. Please select a choice and try again.")
            msgBox.setWindowTitle("Missing item choices")
            msgBox.exec()
        else:
            if count > 1:
                msgBox = QMessageBox()
                #msgBox.exec()
                ### this line executes msgbox as well as sets up its contents
                choice = msgBox.question(self, "Plot FRFs", "You have selected more than one FRF to be plotted. Do you want to plot all of them in the same window?", QMessageBox.Yes | QMessageBox.No)
                msgBox.setIcon(QMessageBox.Question)
                if choice == msgBox.Yes:
                    print("Yes clicked")
                else:
                    print("No clicked")
            else:
                print("Plot only one FRF here")






    @QtCore.pyqtSlot()
    def on_clearSelectionButton_clicked(self):
        self.comboBox.clear()
        self.comboBox.addItems(self.menu)
        self.structureComboBox1.clear()
        self.structureComboBox1.addItem('--Please select--')
        self.structureComboBox2.clear()
        self.structureComboBox2.addItem('--Please select--')
        self.excitationPointComboBox.clear()
        self.excitationPointComboBox.addItem("--Please select--")
        self.responsePointComboBox.clear()
        self.responsePointComboBox.addItem("--Please select--")
        self.FVstructureLabel.setStyleSheet("")
        self.ex_xcheckBox.setChecked(False)
        self.ex_ycheckBox.setChecked(False)
        self.ex_zcheckBox.setChecked(False)
        self.res_xcheckBox.setChecked(False)
        self.res_ycheckBox.setChecked(False)
        self.res_zcheckBox.setChecked(False)
        self.res_rxcheckBox.setChecked(False)
        self.res_rycheckBox.setChecked(False)
        self.res_rzcheckBox.setChecked(False)

        ## removes all checkboxes in the workspace
        for i in reversed(range(self.verticalLayout.count())):
            widgetToRemove = self.verticalLayout.itemAt(i).widget()
            # remove it from the layout list
            self.verticalLayout.removeWidget(widgetToRemove)
            # remove it from the gui
            widgetToRemove.setParent(None)

    @QtCore.pyqtSlot()
    def on_clearWorkspaceButton_clicked(self):
        ## removes all checkboxes in the workspace
        for i in reversed(range(self.verticalLayout.count())):
            widgetToRemove = self.verticalLayout.itemAt(i).widget()
            # remove it from the layout list
            self.verticalLayout.removeWidget(widgetToRemove)
            # remove it from the gui
            widgetToRemove.setParent(None)





def main():
    app=QApplication(sys.argv)
    form=MainApplication()
    form.show()
    app.exec()

if __name__=='__main__':
    main()